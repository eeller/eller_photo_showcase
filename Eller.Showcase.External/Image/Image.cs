﻿using System.Collections.Generic;

namespace Eller.Showcase.External.Image
{
    public class ImageGallery
    {
        public int Id { get; set; }
        public IEnumerable<Image> Images { get; set; }
    }

    public class Image
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string ImageURL { get; set; }
    }
}