﻿using Eller.Showcase.Components.RestClient;
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace Eller.Showcase.External.Image
{
    public class ImageRepository : IImageRepository
    {
        private readonly IRestClient _restClient;

        private static string _rootUrl = "http://someurl";

        public ImageRepository(IRestClient restClient)
        {
            _restClient = restClient;
        }

        public Image GetImage(int galleryId, int id)
        {
            throw new NotImplementedException();
        }

        public ImageGallery GetImageGallery(int galleryId)
        {
            var request = HttpRequestBuilder.Start(_rootUrl)
                .WithParameter("albumId", galleryId.ToString())
                .WithContentType("application/json")
                .WithRequestMethod(HttpMethod.Get)
                .Build();

            var extResult = _restClient.Get(request);
            var imageAlbum = JsonConvert.DeserializeObject<ImageGallery>(extResult.Body);

            return imageAlbum;
        }
    }
}