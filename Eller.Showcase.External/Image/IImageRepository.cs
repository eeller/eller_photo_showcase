﻿namespace Eller.Showcase.External.Image
{
    public interface IImageRepository
    {
        Image GetImage(int galleryId, int id);

        ImageGallery GetImageGallery(int galleryId);
    }
}