﻿using Eller.Showcase.Components.RestClient;
using Eller.Showcase.External.Image;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Net;

namespace Eller.ShowCase.Tests
{
    [TestClass]
    public class ImageRepositoryTests
    {
        private Mock<IRestClient> _restMock;

        private ExternalWebResponse _mockResponse;

        [TestInitialize]
        private void Intialize()
        {
            _restMock = new Mock<IRestClient>();
            _mockResponse = new ExternalWebResponse()
            {
                Body = TestJson,
                Message = string.Empty,
                StatusCode = HttpStatusCode.OK
            };
        }

        [TestCleanup]
        public void CleanUp()
        {
            _restMock = null;
        }

        [TestMethod]
        public void GetImageFromAlbum()
        {
            Intialize();

            var baseLine = new Image
            {
                Id = 1,
                Description = "solutaetharumaliquidofficiisabomnisconsequatur",
                ImageURL = "https://tinyurl.com/y727qzlm"
            };

            _restMock.Setup(x => x.Get(It.IsAny<HttpWebRequest>()))
                .Returns((HttpWebRequest r) => _mockResponse
                );

            var repo = new ImageRepository(_restMock.Object);

            var testAlbum = repo.GetImageGallery(1);

            var testImage = testAlbum.Images.SingleOrDefault(x => x.Id == 1);

            Assert.AreEqual(baseLine.Id, testImage.Id);
            Assert.AreEqual(baseLine.Description, testImage.Description);
            Assert.AreEqual(baseLine.ImageURL, testImage.ImageURL);

            CleanUp();
        }

        #region Test JSON

        private static string TestJson = @"{""Id"":1,""Images"":[{""Id"":1,""Description"":""solutaetharumaliquidofficiisabomnisconsequatur"",""ImageURL"":""https://tinyurl.com/y727qzlm""},{""Id"":2,""Description"":""Thisisthesecondone"",""ImageURL"":""https://tinyurl.com/y727qzlm""}]}";

        #endregion Test JSON
    }
}