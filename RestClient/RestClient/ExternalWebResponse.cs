﻿using System.Net;

namespace Eller.Showcase.Components.RestClient
{
    public class ExternalWebResponse
    {
        public string RequestUrl { get; set; }

        public string Message { get; set; }
        public string Body { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}