﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Eller.Showcase.Components.RestClient
{
    public interface IRestClient
    {
        ExternalWebResponse Get(HttpWebRequest request);

        ExternalWebResponse Post(HttpWebRequest request);

        ExternalWebResponse Put(HttpWebRequest request);

        ExternalWebResponse Delete(HttpWebRequest request);        
    }
}