﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;

namespace Eller.Showcase.Components.RestClient
{
    public class RestClient : IRestClient
    {
        private readonly HttpClient _client;

        public RestClient()
        {
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Add("Accept-Language", "en-US");
            _client.DefaultRequestHeaders.Add("Accept", "application/json, application/xml, text/json, text/x-json, text/javascript, text/xml");
        }

        public ExternalWebResponse Get(HttpWebRequest request)
        {
            return Execute(request);
        }

        public ExternalWebResponse Post(HttpWebRequest request)
        {
            return Execute(request);
        }

        public ExternalWebResponse Put(HttpWebRequest request)
        {
            return Execute(request);
        }

        public ExternalWebResponse Delete(HttpWebRequest request)
        {
            return Execute(request);
        }

        private ExternalWebResponse Execute(HttpWebRequest webRequest)
        {
            var url = webRequest.RequestUri.PathAndQuery;

            ExternalWebResponse response;

            try
            {
                response = GetResponse(webRequest);
                response.RequestUrl = url;

                return response;
            }
            catch (WebException ex)
            {
                if (!(ex.Response is HttpWebResponse))
                {
                    string message;

                    try
                    {
                        message = GetResponseFromStream(ex.Response.GetResponseStream());
                    }
                    catch (Exception ex2)
                    {
                        message = $"{ex.Message} {ex2.Message}";
                    }

                    throw new Exception($"Critical Error: {message} - Request URL: {url}");
                }

                var webResponse = ex.Response as HttpWebResponse;

                response = new ExternalWebResponse
                {
                    StatusCode = webResponse.StatusCode,
                    RequestUrl = url
                };

                try
                {
                    response.Body = GetResponseFromStream(webResponse.GetResponseStream());
                }
                catch (Exception)
                {
                    // Potential for an exception if there is no response stream. This can be safely ignored.
                }

                return response;
            }
        }

        private ExternalWebResponse GetResponse(HttpWebRequest request)
        {
            var webResponse = new ExternalWebResponse();
            var response = request.GetResponse() as HttpWebResponse;
            if (response == null) throw new WebException("Could not get response from stream.", WebExceptionStatus.ReceiveFailure);

            var responseStream = response.GetResponseStream();
            if (responseStream == null) throw new WebException("Could not get response stream from Web Response", WebExceptionStatus.ReceiveFailure);

            webResponse.StatusCode = response.StatusCode;
            webResponse.Message = response.StatusDescription;
            webResponse.Body = GetResponseFromStream(responseStream);
            return webResponse;
        }

        private string GetResponseFromStream(Stream responseStream)
        {
            using (var responseReader = new StreamReader(responseStream))
            {
                var result = responseReader.ReadToEnd();
                responseStream.Close();
                return result;
            }
        }
    }
}