﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace Eller.Showcase.Components.RestClient
{
    public class HttpRequestBuilder
    {
        private NameValueCollection Headers { get; }
        private string BaseUrl { get; set; }
        private string Body { get; set; }
        private string ContentType { get; set; }
        private string Resource { get; set; }
        private HttpMethod Method { get; set; }
        public IDictionary<string, string> Params { get; set; } = new Dictionary<string, string>();

        public HttpRequestBuilder(string url)
        {
            Headers = new NameValueCollection();
            BaseUrl = url;
        }

        public static HttpRequestBuilder Start(string baseUrl) => new HttpRequestBuilder(baseUrl);

        public HttpRequestBuilder WithResource(string resource)
        {
            Resource = resource;
            return this;
        }

        public HttpRequestBuilder WithHeader(string key, string value)
        {
            if (Headers.AllKeys.Contains(key))
            {
                Headers.Set(key, value);
            }
            Headers.Add(key, value);
            return this;
        }

        public HttpRequestBuilder WithContentType(string contentType)
        {
            ContentType = contentType;
            return this;
        }

        public HttpRequestBuilder WithoutHeader(string key)
        {
            if (!Headers.AllKeys.Contains(key)) return this;
            Headers.Remove(key);
            return this;
        }

        public HttpRequestBuilder WithRequestBody(string body)
        {
            Body = body;
            return this;
        }

        public HttpRequestBuilder WithParameter(string name, string value)
        {
            Params.Add(name, value);
            return this;
        }

        public HttpRequestBuilder WithParameters(object obj)
        {
            if (obj == null)
                throw new ArgumentOutOfRangeException();

            var nameValuePairs = obj.ToDictionary();
            foreach (var nameValuePair in nameValuePairs)
            {
                Params.Add(nameValuePair.Key, nameValuePair.Value);
            }

            return this;
        }

        public HttpRequestBuilder WithRequestMethod(HttpMethod verb)
        {
            Method = verb;
            return this;
        }

        public HttpWebRequest Build()
        {
            var request = WebRequest.CreateHttp(AppendQueryStringUrl(GetUrl(), Params, !BaseUrl.Contains("?")));

            request.Method = Method.ToString();

            request.ContentType = ContentType;
            request.Headers.Add(Headers);
            request.Accept = "application/json, application/xml, text/json, text/x-json, text/javascript, text/xml";
            if (request.Method == "GET") return request;

            using (var requestStream = new StreamWriter(request.GetRequestStream()))
            {
                requestStream.Write(Body);
            }

            return request;
        }

        private string GetUrl() => BaseUrl += Resource;

        private static string AppendQueryStringUrl(string pageMethod, IDictionary<string, string> data, bool appendQueryString)
        {
            var i = 0;
            var url = pageMethod;

            if (data == null || data.Count <= 0)
                return url;

            if (appendQueryString)
                url += "?";

            foreach (var value in data)
            {
                if (i > 0)
                    url += "&";
                url += $"{Uri.EscapeDataString(value.Key)}={Uri.EscapeDataString(value.Value)}";
                i++;
            }

            return url;
        }
    }
}