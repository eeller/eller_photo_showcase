﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Eller.Showcase.Components.RestClient
{
    internal static class RestClientExtentions
    {
        internal static IDictionary<string, string> ToDictionary(this object value, string prefix = null)
        {
            if (value == null) return null;
            var dictionary = new Dictionary<string, string>();
            foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(value))
            {
                var ignoreDataMemberAttribute = new IgnoreDataMemberAttribute();
                if (prop.Attributes.Contains(ignoreDataMemberAttribute))
                    continue;

                var propName = prefix + prop.Name;
                var propValue = prop.GetValue(value);

                if (ReferenceEquals(null, propValue) || string.IsNullOrWhiteSpace(propValue.ToString()))
                    continue;
                if (!prop.PropertyType.IsSimple())
                {
                    var dict = propValue.ToDictionary();
                    foreach (var kvp in dict)
                    {
                        if (string.IsNullOrWhiteSpace(kvp.Value))
                            continue;

                        dictionary.Add(kvp.Key, kvp.Value);
                    }
                }
                else
                {
                    dictionary.Add(propName, propValue.ToString());
                }
            }
            return dictionary;
        }

        private static bool IsSimple(this Type type)
        {
            return type.IsValueType || type == typeof(string) || type == typeof(Guid) || type == typeof(DateTime);
        }
    }
}